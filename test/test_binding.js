const Win32Printer = require("../lib/binding.js");
const assert = require("assert");

assert(Win32Printer, "The expected function is undefined");

function testBasic()
{
    const result =  Win32Printer("hello");
    assert.strictEqual(result, "world", "Unexpected value returned");
}

assert.doesNotThrow(testBasic, undefined, "testBasic threw an expection");

console.log("Tests passed- everything looks OK!");