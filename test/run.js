const Win32Printer = require("../lib/binding.js")

// list printers
const printers = Win32Printer.getPrinters().filter(x => x.printProcessor != 'winprint')
console.log('list', printers)

const printerName = '佳能 MP288'

// user options
const options = Win32Printer.getUserDefaultOptions(printerName)
console.log("options", options)

// print doc
const doc = "C:\\Users\\linglq\\Pictures\\Saved Pictures\\测试.jpg",
    myOptions = {
      dpi: 300,
      copies: 1,
      orientation: 'Landscape',   // Portrait, Landscape
      fit: 'contain'              // contain, cover, stretch (default)
    }
const jobId = Win32Printer.printImg(doc, printerName, myOptions);
console.log("jobId", jobId)

// jobs of printer
const jobs = Win32Printer.getJobs(printerName)
console.log("jobs", jobs)
return 0

setInterval(_=>{
  // single job
  if (jobs) {
    jobs.forEach(item => {
      const job = Win32Printer.getJob("Canon MP280 series Printer", item.jobId)
      if (job){
        console.log('============')
        console.log('getJob info: ', job.jobId)
        console.log('document', job.document)
        console.log('statusText', job.statusText)
        console.log('status', job.status)
      } else {
        console.log(`job ${item.jobId} not exists`)
      }
      // list printers
      // const printers = Win32Printer.getPrinters().filter(x => x.printProcessor != 'winprint')
      // console.log('==== printers ====')
      // printers.forEach(item => {
      //   console.log('name: ', item.name)
      //   console.log('status: ', item.status)
      //   console.log('cJobs: ', item.cJobs)
      // })
    })
  }
}, 1000)
